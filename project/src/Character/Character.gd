extends Spatial

export var alive = true
var standing = true

var shake_unit_size = 3
var should_shake = false
var shake = false
onready var shake_tween  = $ShakeTween
var transition_duration = 1.00
var transition_type = 1 # TRANS_SINE
onready var shake_audio = $Body/ShakeAudio
onready var step_audio = $Body/StepAudio
onready var impact_audio = $Body/ImpactAudio

onready var body = $Body
onready var foot_left = $LegL/LegBottom
onready var foot_right = $LegR/LegBottom

onready var ground_raycast = $Body/RayCast
onready var back_raycast = $Body/BackCast
onready var front_raycast = $Body/FrontCast

onready var left_step_particle_location = $LegL/LegBottom/FootCollision/StepParticlesLocation
onready var right_step_particle_location = $LegR/LegBottom/FootCollision/StepParticlesLocation

export var playable : bool = true
export var speed : float = 0
export var foot_speed : float = 4
var turn_speed = 0.1

export var max_lift_force : float = 1

onready var grab_tween = $GrabTween

var move_left_foot : bool = false
var desired_angle = 0.0

func _ready():
	GameManager.set_player_rigidbody($Body)

func kill():
	if alive:
		alive = false
		spawn_particles(BonkParticles_scene, body.global_transform, body.linear_velocity.normalized())

var grabbing = false
var grabbable_node = null
onready var beak_joint = $Body/BeakJoint
func grab():
	if grabbable_node != null:
		beak_joint.set_node_a(body.get_path())
		beak_joint.set_node_b(grabbable_node.get_path())
		grabbing = true
	
func release_grab():
	beak_joint.set_node_a("")
	beak_joint.set_node_b("")
	grabbing = false

func _physics_process(delta):
	if alive and playable:
		if Input.is_action_pressed("grab") and not grabbing:
			grab()
		elif Input.is_action_just_released("grab") and grabbing:
			release_grab()
	
	if grabbing:
		standing = ground_raycast.is_colliding()
	else:
		standing = ground_raycast.is_colliding() or back_raycast.is_colliding() or front_raycast.is_colliding()
	
	if alive and standing:
		var input_vector = Vector3.ZERO
		if playable:
			input_vector = get_input_vector()
	
		if input_vector == Vector3.ZERO:
			should_shake = true
		else:
			should_shake = false
		
		if shake != should_shake:
			shake = should_shake
			if shake:
				shake_tween.interpolate_property(shake_audio, "unit_size", 0, shake_unit_size, 2.0, transition_type, Tween.EASE_IN, 0)
				shake_tween.start()
			else:
				shake_tween.interpolate_property(shake_audio, "unit_size", shake_unit_size, 0, transition_duration, transition_type, Tween.EASE_IN, 0)
				shake_tween.start()
		
		# Could use input_vector.angle()
		if not grabbing:
			if input_vector.z > 0:
				desired_angle = deg2rad(180)
			if input_vector.z < 0:
				desired_angle = deg2rad(0)
			if input_vector.x > 0:
				desired_angle = deg2rad(-90)
			if input_vector.x < 0:
				desired_angle = deg2rad(90)
		
		var lift_force = max_lift_force
		if playable and Input.is_action_pressed("grab") and not grabbing:
			lift_force = 0
		body.apply_central_impulse(Vector3(input_vector.x * speed * delta, lift_force, input_vector.z * speed * delta))
		
		if desired_angle == PI:
			if(body.global_transform.basis.get_euler().y > 0):
				body.apply_torque_impulse(Vector3(0, turn_speed, 0))
			elif(body.global_transform.basis.get_euler().y < 0):
				body.apply_torque_impulse(Vector3(0, -turn_speed, 0))
		else:
			if(body.global_transform.basis.get_euler().y < desired_angle):
				body.apply_torque_impulse(Vector3(0, turn_speed, 0))
			elif(body.global_transform.basis.get_euler().y > desired_angle):
				body.apply_torque_impulse(Vector3(0, -turn_speed, 0))
		
		var foot_lift_speed = 0.6 # 0.55
		if input_vector == Vector3.ZERO:
			foot_lift_speed = 0
		foot_left.apply_central_impulse(
			Vector3(int(move_left_foot) * input_vector.x * foot_speed * delta, -0.75 + int(move_left_foot) * foot_lift_speed, int(move_left_foot) * input_vector.z * foot_speed * delta))
		foot_right.apply_central_impulse(
			Vector3(int(not move_left_foot) * input_vector.x * foot_speed * delta, -0.75 + int(not move_left_foot) * foot_lift_speed, int(not move_left_foot) * input_vector.z * foot_speed * delta))
	else:
		shake_audio.unit_size = 0

func get_input_vector():
	var input_vector = Vector3.ZERO
	input_vector.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	input_vector.z = Input.get_action_strength("move_backward") - Input.get_action_strength("move_forward")
	return input_vector.normalized() if input_vector.length() > 1 else input_vector

func _on_Timer_timeout():
	move_left_foot = not move_left_foot


var StepParticles_scene = preload("res://src/Character/StepParticles.tscn")
var BonkParticles_scene = preload("res://src/Character/BonkParticles.tscn")

func spawn_particles(particles_scene, particles_transform, direction):
	var particles = particles_scene.instance()
	particles.global_transform.origin = particles_transform.origin
	particles.process_material.direction = direction
	owner.add_child(particles)

func _on_LeftLegBottom_body_entered(body):
	if foot_left.linear_velocity.length() > 1.7:
		step_audio.play()
		spawn_particles(StepParticles_scene, left_step_particle_location.global_transform, Vector3(1, 1, 1))

func _on_RightLegBottom_body_entered(body):
	if foot_right.linear_velocity.length() > 1.7:
		step_audio.play()
		spawn_particles(StepParticles_scene, right_step_particle_location.global_transform, Vector3(1, 1, 1))


func _on_Body_body_entered(b):
	if body.linear_velocity.length() > 1.1:
		impact_audio.play()
		if body.linear_velocity.length() > 7.5:
			kill()

func _on_Area_body_entered(body):
	grabbable_node = body

func _on_Area_body_exited(body):
	grabbable_node = null
