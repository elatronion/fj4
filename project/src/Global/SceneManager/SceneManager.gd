extends Spatial

var scene_to_load = "res://scenes/DayLevel.tscn"

func _ready():
	GameManager.connect("load_scene", self, "_on_load_scene")

func _on_load_scene(scene_str):
	scene_to_load = scene_str
	$TransitionScreen.transition()

func _on_TransitionScreen_transitioned():
	var scene = load(scene_to_load)
	$CurrentScene.get_child(0).queue_free()
	$CurrentScene.add_child(scene.instance())
